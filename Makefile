# This python script generates the help for this Makefile.
define PRINT_HELP_PYSCRIPT
from __future__ import print_function
import re, sys

def print_formatted(target, hlp, indent=20):
	print(("%%-%ss %%s" % (indent,)) % (target, hlp))

def print_makefile_help():
	for line in sys.stdin:
		match = re.match(r'^([a-zA-Z_-]+)\s*:.*?## (.*)$$', line)
		if match:
			target, help = match.groups()
			print_formatted(target, help)

if __name__ == "__main__":
	if len(sys.argv) == 1:
		print_makefile_help()
	else:
		print_formatted(*sys.argv[1:])
endef
export PRINT_HELP_PYSCRIPT

# Get default Python version (2 or 3)
PYTHON_MAJOR_VERSION := $(shell python -c 'from __future__ import print_function; import sys; print(sys.version_info.major, end="");')
PYTHON_MINOR_VERSION := $(shell python -c 'from __future__ import print_function; import sys; print(sys.version_info.minor, end="");')

# Choose which virtual environment command to use based on Python version.
# < 3.3 - virtualenv
# >= 3.3. - venv
ifeq ($(PYTHON_MAJOR_VERSION),3)
ifneq ($(filter-out 0 1 2,$(PYTHON_MAJOR_VERSION)),)
VIRTUALENV_CMD := python -m venv
else
VIRTUALENV_CMD := python -m virtualenv
endif
else
VIRTUALENV_CMD := python -m virtualenv
endif

# Determine the location for the development virtualenv.
# If we're already in a virtualenv, use that.
# If the VENV_WORKDIR environment variable is set, use that.
# Else, use 'venv'
CURRENT_VENV := $(shell python -c 'from __future__ import print_function; import sys; print(sys.prefix if hasattr(sys, "real_prefix") or (hasattr(sys, "base_prefix") and sys.base_prefix != sys.prefix) else "", end="")')
VENV_WORKDIR ?= .
ifeq ($(CURRENT_VENV),)
# We not already in a virtualenv, so make one.
VIRTUALENV := $(VENV_WORKDIR)/venv
else
VIRTUALENV := $(CURRENT_VENV)
endif

WITHVENV := source $(VIRTUALENV)/bin/activate &&

help:
	@echo "Commands:"
	@echo ""
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)
	@echo ""
	@echo ""
	@echo "Environment Variables:"
	@echo ""
	@python -c "$$PRINT_HELP_PYSCRIPT" VENV_WORKDIR "Create the development virtualenv in this directory"
	@echo ""
.PHONY: help
.DEFAULT_GOAL := help

env: $(VIRTUALENV) ## create development virtualenv
.PHONY: env
$(VIRTUALENV): $(VIRTUALENV)/bin/activate
$(VIRTUALENV)/bin/activate: requirements.txt
	test -d $(VIRTUALENV) || $(VIRTUALENV_CMD) $(VIRTUALENV)
	$(VIRTUALENV)/bin/pip install -U six setuptools pip
	$(VIRTUALENV)/bin/pip install -U -r requirements.txt
	touch $(VIRTUALENV)/bin/activate

build-docs: $(VIRTUALENV) ## Build HTML docs into the `site/` dir
	$(WITHVENV) mkdocs build
.PHONY: build-docs

serve-docs: $(VIRTUALENV) ## Serve docs locally
	$(WITHVENV) mkdocs serve
.PHONY: serve-docs

clean: clean-venv clean-site
.PHONY: clean

clean-site:
	rm -rf site
.PHONY: clean-site

clean-venv:
	rm -rf venv
.PHONY: clean-venv
