# Python Library Versioning (with Git and Continuous Integration)

Python has a guide for version numbers: [PEP-440](https://www.python.org/dev/peps/pep-0440/)

This is the guide to that guide for Mintel. It seeks to expand on PEP-440 by providing advice and best practices.
